package com.dt3264.deezloader;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    public static final String serverURL = "http://localhost:1730";
    //Observer which receives and handles the data
    public static Observer<Message> mainDataHandler;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
        System.loadLibrary("node");
    }

    // Force reloading node data every time the app starts
    final boolean forceReloadNodeData = false;
    // Build number of the running app
    final int clientBuildNumber = BuildConfig.VERSION_CODE;
    final String clientVersion = BuildConfig.VERSION_NAME;
    final String telegramURL = "https://t.me/joinchat/Ed1JxEfoci-dp-BWGRdVLg";
    int lastCompile;
    boolean landingDone = false;
    SharedPreferences sharedPreferences;

    Timer checkTick = new Timer();

    // UI elements
    Context context;
    Snackbar snackbar;
    @BindView(R.id.openAppExternalButton)
    Button mainExternalButton;
    @BindView(R.id.openAppInternalButton)
    Button mainInternalButton;
    @BindView(R.id.telegramButton)
    Button telegramButton;
    @BindView(R.id.updateButton)
    Button updateButton;
    @BindView(R.id.infoView)
    TextView infoView;
    @BindView(R.id.updateTxt)
    TextView updateTxt;
    @BindView(R.id.versionView)
    TextView versionView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    String[] pasteResult;
    OkHttpClient client = new OkHttpClient();
    private Observer<Message> _mainDataHandler = new Observer<Message>() {
        @Override
        public void onSubscribe(Disposable d) {
        }

        @Override
        public void onNext(Message msg) {
            if (msg.getType() == 4) {
                finishAndRemoveTask();
            }
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onComplete() {
        }
    };

    private static boolean copyAssetFolder(AssetManager assetManager, String fromAssetPath, String toPath) {
        try {
            String[] files = assetManager.list(fromAssetPath);
            boolean res = true;

            if (Objects.requireNonNull(files).length == 0) {
                //If it's a file, it won't have any assets "inside" it.
                res = copyAsset(assetManager, fromAssetPath, toPath);
            } else {
                new File(toPath).mkdirs();
                for (String file : files)
                    res &= copyAssetFolder(assetManager, fromAssetPath + "/" + file,
                            toPath + "/" + file);
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean copyAsset(AssetManager assetManager, String fromAssetPath, String toPath) {
        InputStream in;
        OutputStream out;
        try {
            in = assetManager.open(fromAssetPath);
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            in.close();
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    @SuppressWarnings("JniMissingFunction")
    public static native Integer startNodeWithArguments(String[] arguments);

    private static boolean deleteFolderRecursively(File file) {
        try {
            boolean res = true;
            for (File childFile : file.listFiles()) {
                if (childFile.isDirectory()) {
                    res &= deleteFolderRecursively(childFile);
                } else {
                    res &= childFile.delete();
                }
            }
            res &= file.delete();
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void copyFile(InputStream in, OutputStream out) {
        try {
            byte[] buffer = new byte[4096];
            int read;
            while ((read = in.read(buffer, 0, 4096)) != -1) {
                out.write(buffer, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        prepareHandler();
        infoView.setText(getText(R.string.serverLoadingText));
        versionView.setText(getText(R.string.actualVersion) + " " + clientVersion);
        telegramButton.setOnClickListener(view -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(telegramURL));
            startActivity(i);
        });
        mainExternalButton.setOnClickListener(view -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(serverURL));
            startActivity(i);
        });
        mainInternalButton.setOnClickListener(view -> {
            Intent i = new Intent(getApplicationContext(), BrowserActivity.class)
                    .setAction(Intent.ACTION_MAIN)
                    .addCategory(Intent.CATEGORY_LAUNCHER)
                    .addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            startActivity(i);
        });
        snackbar = Snackbar.make(infoView, "Preparing server data", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
        run();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        lastCompile = sharedPreferences.getInt("lastCompile", 0);
        checkPermissions();
    }

    // Main methods
    void checkPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
    }

    void readyLandingPage() {
        context = this;
        runOnUiThread(() -> {
            Button telegramButton = findViewById(R.id.telegramButton);
            Button externalButton = findViewById(R.id.openAppExternalButton);
            Button internalButton = findViewById(R.id.openAppInternalButton);
            ProgressBar progressBar = findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            telegramButton.setVisibility(View.VISIBLE);
            externalButton.setVisibility(View.VISIBLE);
            internalButton.setVisibility(View.VISIBLE);
            snackbar.dismiss();
            infoView.setText(getText(R.string.serverReady));
        });
        landingDone = true;
    }

    void run() {
        Request request = new Request.Builder().url("https://pastebin.com/raw/rEubX2Lu").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                pasteResult = Objects.requireNonNull(response.body()).string().split("\n");
                if (!clientVersion.equals(pasteResult[0].trim())) {
                    runOnUiThread(() -> {
                        updateTxt.setText("A new update (" + pasteResult[0].trim() +
                                ") is available.\nChanges:\n" + pasteResult[2]);
                        updateButton.setOnClickListener(view -> {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(pasteResult[1]));
                            startActivity(i);
                        });
                        TextView updateTxt = findViewById(R.id.updateTxt);
                        ViewGroup.LayoutParams params = updateTxt.getLayoutParams();
                        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        updateTxt.setLayoutParams(params);
                        updateButton.setVisibility(View.VISIBLE);
                    });
                }
            }
        });
    }

    void prepareHandler() {
        mainDataHandler = _mainDataHandler;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        checkTick.cancel();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(getString(R.string.exit_back));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.confirmation, (dialogInterface, i) -> {
            finish();
            System.exit(0);
        });
        builder.setNegativeButton(R.string.denial, (dialogInterface, i) -> dialogInterface.cancel());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void saveLastUpdateTime() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("lastCompile", clientBuildNumber);
        editor.apply();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    checkTick.schedule(new checkServer(), 0, 10000);
                } else {
                    // permission denied, boo!
                    Toast.makeText(this, R.string.permission,
                            Toast.LENGTH_SHORT).show();
                    this.finish();
                }
            }
        }
    }

    /**
     * Check server files methods
     */
    private void checkIsApkUpdated() {
        if (forceReloadNodeData || lastCompile != clientBuildNumber) {
            //Recursively delete any existing nodejs-project.
            String nodeDir = getApplicationContext().getFilesDir().getAbsolutePath() + "/deezerLoader";
            File nodeDirReference = new File(nodeDir);
            if (nodeDirReference.exists()) {
                deleteFolderRecursively(new File(nodeDir));
            }

            //Copy the node project from assets into the application's data path.
            copyAssetFolder(getApplicationContext().getAssets(), "deezerLoader", nodeDir);
            saveLastUpdateTime();
        }
    }

    class checkServer extends TimerTask {
        public void run() {
            if (!MyService.isServiceRunning) {
                new Thread(() -> {
                    checkIsApkUpdated();
                    runOnUiThread(() -> snackbar.setText(R.string.start_server));
                    Intent startIntent = new Intent(getApplicationContext(), MyService.class);
                    startIntent.setAction(getString(R.string.serviceName));
                    startService(startIntent);
                }).start();
                while (!landingDone) {
                    if (MyService.isServiceRunning) {
                        readyLandingPage();
                        break;
                    }
                }
            }
        }
    }
}
